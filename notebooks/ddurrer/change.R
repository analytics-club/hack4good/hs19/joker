# Hack4Good 2/11/2019
# record changes in prices compared to the previous month

### Set paths ###
input_data_path <- "C:/Users/damdu/polybox/Hack4Good/Input_Data"
output_data_path <- "C:/Users/damdu/polybox/Hack4Good/Output_Data"

# Load libraries
library(tidyverse)

# load data
cockpit <- read.csv(paste(output_data_path, "cockpit_file_agg_naintrap_all.csv", sep = "/"))[,-1]

# sort and convert data
cockpit <- cockpit %>% arrange(q_sbd, month) %>% type.convert(as.is = T)

# set up vectors to loop through
items <- colnames(cockpit)[str_detect(colnames(cockpit), "Price")]
sbds <- unique(cockpit$q_sbd)
months <- unique(cockpit$month)

# record changes for all items in all sub-districts for all months (where available)
# this takes around 20min to run

#record time of outer loop
start_time_outer <- Sys.time()

for(i in items){
  
  #record time of inner loop
  start_time_inner <- Sys.time()
  
  # select item
  cockpit_item <- cockpit %>% select(one_of("q_sbd", "month", i))
  
  # initialize change frame for this item
  cockpit_item_change <- data.frame()
  
  for(s in sbds){
    
    # select sub-district
    cockpit_item_sbd <- cockpit_item %>% filter(q_sbd == s)
    
    # initialize events frame for this subdistrict
    change_item_sbd <- c()
    
    for(m in months){
      
      #record changes
      if(m == 1){
        change_item_sbd[1] <- NA
      } else{
        prev <- as.numeric(cockpit_item_sbd %>% filter(month == m - 1) %>% select(i))
        curr <- as.numeric(cockpit_item_sbd %>% filter(month == m) %>% select(i))
        change_item_sbd[m] <- log(curr) - log(prev)
      }

    }
    
    # add data from this month to sub_district 
    cockpit_item_sbd <- data.frame(cockpit_item_sbd, change_item_sbd)
    
    # add data from this sub_district to item
    cockpit_item_change <- rbind(cockpit_item_change, cockpit_item_sbd)
  }
  
  # add data from this item to cockpit
  cockpit <- cockpit %>% 
    left_join(cockpit_item_change, by = c("q_sbd","month",i)) %>% 
    rename_at(vars("change_item_sbd"), 
              funs(str_replace(., "change_item_sbd", paste("delta_", i, sep = ""))))
  
  #record time inner loop
  end_time_inner <- Sys.time()
  time_to_run_inner <- end_time_inner - start_time_inner
}

#record time outer loop
end_time_outer <- Sys.time()
time_to_run_outer <- end_time_outer - start_time_outer

write.csv(cockpit, paste(output_data_path, "cockpit_file_agg_naintrap_change.csv", sep = '/'))
