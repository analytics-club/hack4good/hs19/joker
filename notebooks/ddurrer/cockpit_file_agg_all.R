# Hack4Good Hackday 1 5/10/2019
# Cockpit file aggregation for all sbds, months and items

### Set paths ###
input_data_path <- "C:/Users/damdu/polybox/Hack4Good/Input_Data"
output_data_path <- "C:/Users/damdu/polybox/Hack4Good/Output_Data"

# Load libraries
library(tidyverse)
library(readxl)

########################
# Load and set up data
########################


#read aggregated data on subdistrict level
read_excel_allsheets <- function(filename, tibble = T) {
  sheets <- readxl::excel_sheets(filename)
  x <- lapply(sheets, function(X) readxl::read_excel(filename, sheet = X))
  if(!tibble) x <- lapply(x, as.data.frame)
  names(x) <- sheets
  x
}

data_agg <- read_excel_allsheets(paste(input_data_path, "reach_syr_dataset_market monitoring_redesign_august2019_subdistrict_timeseries.xlsx", sep = '/'))

subdistrict_data <- data_agg$'Subdistrict_Time Series'

names(subdistrict_data) <- as.character(subdistrict_data[1,])

subdistrict_data <- subdistrict_data[-1,]


#choose relevant dates
dates <- unique(subdistrict_data[,1])[13:nrow(unique(subdistrict_data[,1])),]

#subset subdistrict timeseries on relevant dates
subdistrict_data <- subdistrict_data[subdistrict_data$month2 %in% pull(dates),]

#get only subdistrictswhich are consistently filled during months in scope
subdistricts <- count(subdistrict_data, q_sbd) %>% select(q_sbd)

#subset subdistrict timeseries on subdistricts
subdistrict_data <- subdistrict_data[subdistrict_data$q_sbd %in% pull(subdistricts),]

#convert month to numbers from 1 to 22
months <- cbind(dates, c(1:nrow(dates)))
names(months) <- c("month2", "month")

subdistrict_data <- left_join(subdistrict_data, months, by = "month2")



########################
# data quality checks and cleansing
########################

# Find inconsistent q_sbd / admin3Names_en combinations
name_sbd <- subdistrict_data[!is.na(subdistrict_data$admin3Name_en),] %>%
  group_by(q_sbd) %>%
  summarise(unique_sbd_names = n_distinct(admin3Name_en)) %>%
  arrange(desc(unique_sbd_names))

print(name_sbd)

all(name_sbd$unique_sbd_names == 1)
## one name wrong

name_sbd_problem <- data.frame(name_sbd$q_sbd[name_sbd$unique_sbd_names > 1], table(subdistrict_data$admin3Name_en[subdistrict_data$q_sbd == name_sbd$q_sbd[name_sbd$unique_sbd_names > 1]]))
colnames(name_sbd_problem) <- c("q_sbd", "admin3Name_en", "Freq")
## one name wrong

#subdistrict_data[subdistrict_data$admin3Name_en==name_sbd_problem$admin3Name_en[name_sbd_problem$Freq == min(name_sbd_problem$Freq)],]

#remove rows where q_sbd is assigned to the wrong district or admin3Name_en (we don't know which one is wrong)
subdistrict_data <- subdistrict_data %>%
  filter(!(q_sbd == name_sbd_problem$q_sbd[name_sbd_problem$Freq == min(name_sbd_problem$Freq)] &
             admin3Name_en == name_sbd_problem$admin3Name_en[name_sbd_problem$Freq == min(name_sbd_problem$Freq)]))


# Find inconsistent q_district / admin2Names_en combinations
name_dist <- subdistrict_data[!is.na(subdistrict_data$admin2Name_en),] %>%
  group_by(q_district) %>%
  summarise(unique_dist_names = n_distinct(admin2Name_en)) %>%
  arrange(desc(unique_dist_names))

print(name_dist)

all(name_dist$unique_dist_names == 1)
## no inconsistencies

# Find inconsistent q_gov / admin1Names_en combinations
name_gov <- subdistrict_data[!is.na(subdistrict_data$admin1Name_en),] %>%
  group_by(q_gov) %>%
  summarise(unique_gov_names = n_distinct(admin1Name_en)) %>%
  arrange(desc(unique_gov_names))

print(name_gov)

all(name_gov$unique_gov_names == 1)
## no inconsistencies


# Find inconsistent q_district / q_sbd combinations
sbd_dist <- subdistrict_data[!is.na(subdistrict_data$q_district),] %>%
  group_by(q_sbd) %>%
  summarise(unique_dist = n_distinct(q_district)) %>%
  arrange(desc(unique_dist))

print(sbd_dist)

all(sbd_dist$unique_dist == 1)
##two combinations wrong

dist_sbd_problem <- data.frame()
for(i in sbd_dist$q_sbd[sbd_dist$unique_dist > 1]){
  dist_sbd_problem_temp <- data.frame(i, table(subdistrict_data$q_district[subdistrict_data$q_sbd %in% i]))
  colnames(dist_sbd_problem_temp) <- c("q_sbd", "q_district", "Freq")
  dist_sbd_problem <- rbind(dist_sbd_problem, dist_sbd_problem_temp)
}


#remove rows where q_sbd is assigned to the wrong district (we don't know which one is wrong)
for(i in sbd_dist$q_sbd[sbd_dist$unique_dist > 1]){
  dist_sbd_problem_temp <- dist_sbd_problem[dist_sbd_problem$q_sbd == i,]
  subdistrict_data <- subdistrict_data %>%
    filter(!(q_sbd == dist_sbd_problem_temp$q_sbd[dist_sbd_problem_temp$Freq == min(dist_sbd_problem_temp$Freq)] &
               q_district == dist_sbd_problem_temp$q_district[dist_sbd_problem_temp$Freq[dist_sbd_problem_temp$q_sbd == i] == min(dist_sbd_problem_temp$Freq)]))
}


# Find inconsistent q_district / q_gov combinations
dist_gov <- subdistrict_data[!is.na(subdistrict_data$q_gov),] %>%
  group_by(q_district) %>%
  summarise(unique_gov = n_distinct(q_gov)) %>%
  arrange(desc(unique_gov))

print(dist_gov)

all(dist_gov$unique_gov == 1)
##no problems




########################
#create a complete dataset for all subdistricts in all months with NA where appropriate
########################

#select data encoding governorate, district and sub-district
geodata <- select(subdistrict_data, c("region",
                                      "q_gov",
                                      "admin1Name_en",
                                      "q_district",
                                      "admin2Name_en",
                                      "q_sbd",
                                      "admin3Name_en")) %>%
            distinct() %>%
            filter(q_sbd %in% pull(subdistricts))


#create a grid with governorate, district and sub-district at each month
full_data <- tbl_df(expand.grid(pull(subdistricts), c(1:nrow(dates)))) %>%
  rename(q_sbd = Var1, month = Var2) %>%
  left_join(geodata, by = "q_sbd")
   

#join the data to the grid                               
full_data <- left_join(tbl_df(select(full_data, -one_of(c("region",
                                                          "q_gov",
                                                          "admin1Name_en",
                                                          "q_district",
                                                          "admin2Name_en",
                                                          "admin3Name_en"))))
                       , tbl_df(subdistrict_data), by = c("month", "q_sbd" ))


#set NA where appropriate
full_data[full_data == "NA"] <- NA

#convert data to appropriate types
full_data <- type.convert(full_data)

#remove duplicate variables
full_data <- select(full_data, -ends_with(".x"))


#write file
write.csv(full_data, paste(output_data_path, "cockpit_file_agg_all.csv", sep = '/'))
