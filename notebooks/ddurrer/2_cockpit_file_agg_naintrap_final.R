# Hack4Good Hackday 1 5/10/2019
# Interpolate missing values for all columbs where we have at least 7 values (intrapolate max 3 consecutive NA values)
# 
# 1. Load file "cockpit_file_agg_all.csv" (output from script "cockpit_file_all_final.R")
# 2. Linearly interpolate missing price values for subdistricts where at least 7 values are available (max 3 consecutive missing values)
# 3. OUtput "cockpit_file_agg_naintrap.csv"

### Set paths ###
input_data_path <- "C:/Users/damdu/polybox/Hack4Good/Input_Data"
output_data_path <- "C:/Users/damdu/polybox/Hack4Good/Output_Data_final"

# Load libraries
library(tidyverse)
library(zoo)

# load data
cockpit <- read.csv("C:/Users/damdu/polybox/Hack4Good/Output_Data/cockpit_file_agg_all.csv")

# sort data
cockpit <- arrange(cockpit, q_sbd, month)

#initialize new frame
full_cockpit <- data.frame()


for(j in unique(cockpit$q_sbd)){
  
  #select sub-district
  cockpit_temp <- cockpit[cockpit$q_sbd == j,]
  
  for(i in 19:ncol(cockpit_temp)){
    
    #condition on min 7 values for intrapolation
    if(sum(!is.na(cockpit_temp[,i])) > 7){
      
      #perform lineat intrapolation of max 3 consecutive missing values
      cockpit_temp[,c(3,i)] <- na.approx(arrange(cockpit_temp[,c(3,i)], month), na.rm = F, maxgap = 3)
      
    }
    
  }
  
  #add intrapolated values to full cockpit
  full_cockpit <- rbind(full_cockpit, cockpit_temp)
}

#convert data to appropriate type
full_cockpit <- type.convert(full_cockpit)[,-1]

#write file
write.csv(full_cockpit, paste(output_data_path, "cockpit_file_agg_naintrap.csv", sep = '/'))
